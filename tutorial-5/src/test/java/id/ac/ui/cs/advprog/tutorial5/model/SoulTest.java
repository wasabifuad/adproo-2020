package id.ac.ui.cs.advprog.tutorial5.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class SoulTest {

    private Soul soul;

    @BeforeEach
    public void setUp(){
        soul = new SoulBuilder().setName("Ijug").setAge(21).setGender("M").setOccupation("Petani").createSoul();
        soul.setId(1);
    }

    @Test
    void getName() {
        assertEquals("Ijug",soul.getName());
    }

    @Test
    void setName() {
        soul.setName("Ijug.gg");
        assertEquals("Ijug.gg",soul.getName());
    }

    @Test
    void getAge() {
        assertEquals(21,soul.getAge());
    }

    @Test
    void setAge() {
        soul.setAge(22);
        assertEquals(22,soul.getAge());
    }

    @Test
    void getGender() {
        assertEquals("M",soul.getGender());
    }

    @Test
    void setGender() {
        soul.setGender("L");
        assertEquals("L",soul.getGender());
    }

    @Test
    void getOccupation() {
        assertEquals("Petani",soul.getOccupation());
    }

    @Test
    void setOccupation() {
        soul.setOccupation("Polisi");
        assertEquals("Polisi",soul.getOccupation());
    }

    @Test
    void getId() {
        assertEquals(1,soul.getId());
    }

    @Test
    void setId() {
        soul.setId(2);
        assertEquals(2,soul.getId());
    }
}