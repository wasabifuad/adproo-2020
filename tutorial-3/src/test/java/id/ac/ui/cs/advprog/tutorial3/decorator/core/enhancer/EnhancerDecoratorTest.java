package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;



public class EnhancerDecoratorTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;
    Weapon weapon5;
    @Test
    public void testAddWeaponEnhancement(){

        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon4);
        weapon5 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon5);

        //TODO: Complete me
        int upgradeValue = weapon3.getWeaponValue();
        assertTrue(upgradeValue >= 1 && upgradeValue <= 5);
        upgradeValue = weapon4.getWeaponValue();
        assertTrue(upgradeValue >= 5 && upgradeValue <= 10);
        upgradeValue = weapon5.getWeaponValue();
        assertTrue(upgradeValue >= 10 && upgradeValue <= 15);
        upgradeValue = weapon2.getWeaponValue();
        assertTrue(upgradeValue >= 15 && upgradeValue <= 20);
        upgradeValue = weapon1.getWeaponValue();
        assertTrue(upgradeValue >= 50 && upgradeValue <= 55);
    }

}
