package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;
    int upgradedValue;

    public RawUpgrade(Weapon weapon) {

        this.weapon= weapon;

    }

    @Override
    public String getName() {
        return "Raw " +weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        Random rand = new Random();
        int randomNum = rand.nextInt((10 - 5) + 1) + 5;
        upgradedValue = randomNum;
        return (weapon != null ? weapon.getWeaponValue() : 0) + upgradedValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Raw "+weapon.getDescription();
    }
}
