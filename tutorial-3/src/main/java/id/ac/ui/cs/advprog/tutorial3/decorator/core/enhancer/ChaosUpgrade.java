package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;



import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    int upgradedValue;

    public ChaosUpgrade(Weapon weapon) {
        this.weapon= weapon;

    }

    @Override
    public String getName() {
        return "Chaos " +weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        Random rand = new Random();
        int randomNum = rand.nextInt((55 - 50) + 1) + 50;
        upgradedValue = randomNum;
        return (weapon != null ? weapon.getWeaponValue() : 0) + upgradedValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Chaos "+weapon.getDescription();
    }
}
