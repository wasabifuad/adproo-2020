package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;
    int upgradedValue;

    public UniqueUpgrade(Weapon weapon){

        this.weapon = weapon;

    }

    @Override
    public String getName() {
        return "Unique " +weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        Random rand = new Random();
        int randomNum = rand.nextInt((15 - 10) + 1) + 10;
        upgradedValue = randomNum;
        return (weapon != null ? weapon.getWeaponValue() : 0) + upgradedValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Unique "+weapon.getDescription();
    }
}
