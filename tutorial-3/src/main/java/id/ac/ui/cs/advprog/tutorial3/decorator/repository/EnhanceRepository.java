package id.ac.ui.cs.advprog.tutorial3.decorator.repository;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer.EnhancerDecorator;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class EnhanceRepository {

    public void enhanceToAllWeapons(ArrayList<Weapon> weapons){
        Weapon weaponUpgrade;
        int index;
        for (Weapon weapon: weapons) {
            switch (weapon.getName()) {
                case "Gun":
                    weaponUpgrade = WeaponProducer.WEAPON_GUN.createWeaponEnhancer();
                    weaponUpgrade = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weaponUpgrade);
                    weaponUpgrade = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weaponUpgrade);
                    index = weapons.indexOf(weapon);
                    weapons.set(index,weaponUpgrade);
                    break;

                //TODO: Complete me
                case "Longbow":
                    weaponUpgrade = WeaponProducer.WEAPON_LONGBOW.createWeaponEnhancer();
                    weaponUpgrade = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weaponUpgrade);
                    weaponUpgrade = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weaponUpgrade);
                    weaponUpgrade = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weaponUpgrade);
                    index = weapons.indexOf(weapon);
                    weapons.set(index,weaponUpgrade);
                    break;

                case "Shield":
                    weaponUpgrade = WeaponProducer.WEAPON_SHIELD.createWeaponEnhancer();
                    weaponUpgrade = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weaponUpgrade);
                    weaponUpgrade = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weaponUpgrade);
                    index = weapons.indexOf(weapon);
                    weapons.set(index,weaponUpgrade);
                    break;

                case "Sword":
                    weaponUpgrade = WeaponProducer.WEAPON_SWORD.createWeaponEnhancer();
                    weaponUpgrade = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weaponUpgrade);
                    weaponUpgrade = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weaponUpgrade);
                    index = weapons.indexOf(weapon);
                    weapons.set(index,weaponUpgrade);
                    break;

            }
        }
    }
}
